const express = require('express');
const router = express.Router();


/******************************************************** */

/**
 * Metodo que lanza el index
 */
router.get('/',(req, res) =>{
    res.render('layouts/inicio/inicio');
});

module.exports = router;
